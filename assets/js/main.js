$(document).ready(function(){
    ////////////////////////////////////////////
    function move_desk_mobile(item, mobile, mq){
        if ( window.innerWidth  <= mq && !$(item).hasClass('move-changed')) {
            $(mobile).append($(item));
            $(item).addClass('move-changed');
        }
    }
    
    ////////////////////////////////////////////
    function back_mobile_desktop(item, desktop, mq){
        if ( window.innerWidth  > mq && $(item).hasClass('move-changed')) {
            $(desktop).append($(item));
            $(item).removeClass('move-changed');
        }
    }

    ////////////////////////////////////////////
    function new_carousel_products(){
        var  mini
        var  maxi
        var  medi
        if ( window.innerWidth  > 639){
            mini = 4;
            maxi = 4;
            medi = 250;
        }
        else{
            mini = 2;
            maxi = 2;
            medi = 500;
        }
        $('#ju_landing .category-container .products .carousel-container').each(function(index){
            $(this).bxSlider({
                minSlides: mini,
                maxSlides: maxi, 
                slideWidth: medi, 
                prevSelector: $(this).parent().find('.prev'),
                nextSelector: $(this).parent().find('.next'),
                responsive: true,
                infiniteLoop: false,
                nextText: '',
                prevText: '',
                pager: false,
            });
        });
    }
    function carousel_products(){
        $('#ju_landing .ju-carousel .ju-carousel-container').on('init', function(slick){
            //console.log('init');
            // left
        });
        $('#ju_landing .category-container .products .carousel-container').each(function(index){
            $(this).slick({
                dots: false,
                arrows: true,
                slidesToShow: 4,
                slidesToScroll: 1,
                infinite: false,
                /* autoplay: true,
                autoplaySpeed: 3000, */
                responsive: [
                    {breakpoint: 992,settings: {slidesToShow: 3,}},
                    {breakpoint: 639,settings: {slidesToShow: 2,}},
                    //{breakpoint: 450,settings: {slidesToShow: 1,}},
                ],
            });
        });
    }

    ////////////////////////////////////////////
    function smooth_scroll(){
        var easeInOutQuad = new SmoothScroll('[data-easing="easeInOutQuad"]', {easing: 'easeInOutQuad', offset: 0});
    }

    ////////////////////////////////////////////
    function scrollspy(){
        /* Variables */        
        var item = document.getElementById("ju_header");
        var global_offset = item.offsetTop;
        /* funcion del menu */
        function scrollspy_header(){
            if( window.pageYOffset >= global_offset && window.innerWidth  > 991){
                item.classList.add("sticky");
            }else if( window.innerWidth  > 991 ){
                item.classList.remove("sticky");
            }
        };
        /* windows on scroll global */
        window.onscroll = function() {
            scrollspy_header();
        };
    }
    ////////////////////////////////////////////
    function wow(){
        var wow = new WOW({
            boxClass: 'ju-section',
            animateClass: '',
            offset: 500,
            callback: function(box){
                console.log('w');
                $(box).find('.ju-banner').addClass('animated');
                $(box).find('.ju-carousel').addClass('animated');
            },
        });
        wow.init();
    }; 

    ////////////////////////////////////////////
    new_carousel_products();
    smooth_scroll();
    //scrollspy();
    //wow();
    //EJECUCION DE FUNCIONES (Rezize)
    window.addEventListener('resize', function () {
    
    });
});

////////////////////////////////////////////
$( window ).on( "load", function(){
        
}); 



