// npm install --save-dev gulp gulp-autoprefixer browser-sync gulp-sass gulp-cssmin gulp-group-css-media-queries
'use strict'
const gulp = require('gulp')
const browserSync = require('browser-sync').create()
const reload = browserSync.reload
const pug = require('gulp-pug');
const sass = require('gulp-sass')
const rename = require('gulp-rename')
const autoprefixer = require('gulp-autoprefixer')
const cssmin = require('gulp-cssmin')
const mediaq = require('gulp-group-css-media-queries')

gulp.task('server', () => {
	browserSync.init({
		server: {
			baseDir: './'
		}
	})
})
gulp.task('libs', () => {
	return gulp.src('./assets/css/scss/libs.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({ browsers: ['> 5%', 'ie 8'] }))
	.pipe(mediaq())
	.pipe(cssmin())
	.pipe(rename({suffix: '.min',}))
	.pipe(gulp.dest('./assets/css/'))
	.pipe(browserSync.stream())
})
gulp.task('assets', () => {
	return gulp.src('./assets/css/scss/main.scss')
	.pipe(sass().on('error', sass.logError))
	.pipe(autoprefixer({ browsers: ['> 5%', 'ie 8'] }))
	.pipe(mediaq())
	.pipe(cssmin())
	.pipe(rename({suffix: '.min',}))
	.pipe(gulp.dest('./assets/css/'))
	.pipe(browserSync.stream())
})
gulp.task('watch', () => {
	gulp.watch('./*.html').on('change', reload)
	gulp.watch('./assets/js/*.js').on('change', reload)
	gulp.watch('./assets/css/scss/*.scss', ['assets'])
	gulp.watch('./assets/css/scss/**/*.scss', ['assets'])
})
gulp.task('default', ['server', 'libs', 'assets', 'watch'])